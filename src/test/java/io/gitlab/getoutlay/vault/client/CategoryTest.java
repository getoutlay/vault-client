package io.gitlab.getoutlay.vault.client;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;

class CategoryTest {

  @Test
  void getIdSetId() {
    Category category = new Category();
    assertNull(category.getId());

    category.setId(42L);
    assertEquals(42L, category.getId());
  }

}