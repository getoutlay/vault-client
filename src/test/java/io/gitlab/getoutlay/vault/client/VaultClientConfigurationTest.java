package io.gitlab.getoutlay.vault.client;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest(webEnvironment = WebEnvironment.NONE)
@ActiveProfiles("test")
class VaultClientConfigurationTest {

  @Autowired
  VaultClientConfiguration configuration;

  @Test
  void isNotNull() {
    assertNotNull(configuration);
  }

  @Test
  void getProperties() {
    assertNotNull(configuration.getProperties());
    assertEquals("http://some-url", configuration.getProperties().getUrl());
  }

  @Test
  void getObjectMapper() {
    assertNotNull(configuration.getObjectMapper());
  }

  @Test
  void getVaultServiceUrl() {
    assertNotNull(configuration.getVaultServiceUrl());
    assertEquals("http://some-url", configuration.getVaultServiceUrl());
  }
}