package io.gitlab.getoutlay.vault.client;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;

class TransactionTest {

  @Test
  void getIdSetId() {
    Transaction transaction = new Transaction();
    assertNull(transaction.getId());

    transaction.setId(42L);
    assertEquals(42L, transaction.getId());
  }

}