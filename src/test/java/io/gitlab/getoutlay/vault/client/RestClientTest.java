package io.gitlab.getoutlay.vault.client;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.http.MediaType.APPLICATION_JSON;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.codec.ClientCodecConfigurer.ClientDefaultCodecs;
import org.springframework.http.codec.json.Jackson2JsonDecoder;
import org.springframework.http.codec.json.Jackson2JsonEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;

@SpringBootTest(webEnvironment = WebEnvironment.NONE)
@ActiveProfiles("test")
class RestClientTest {

  @Autowired
  ObjectMapper objectMapper;

  public RestClient<Dummy> createRestClient(String rootUrl) {
    ExchangeStrategies strategies = ExchangeStrategies.builder()
        .codecs(clientDefaultCodecs -> {
          ClientDefaultCodecs defaultCodecs = clientDefaultCodecs.defaultCodecs();
          defaultCodecs.jackson2JsonEncoder(new Jackson2JsonEncoder(objectMapper, APPLICATION_JSON));
          defaultCodecs.jackson2JsonDecoder(new Jackson2JsonDecoder(objectMapper, APPLICATION_JSON));
        })
        .build();
    WebClient client = WebClient.builder()
        .exchangeStrategies(strategies)
        .baseUrl(rootUrl)
        .build();
    return new RestClient<>(client, Dummy.class, "dummies");
  }

  @Test
  public void listAll() {
    List<Dummy> list = Arrays.asList(new Dummy("a"), new Dummy("b"));
    MockServer server = MockServer.start(objectMapper);
    String rootUrl = server.rootUrl() + "";
    server.get("/dummies")
        .expect()
        .status(HttpStatus.OK)
        .contentType(MediaType.APPLICATION_JSON)
        .body(list)
        .enqueue();

    List<Dummy> result = createRestClient(rootUrl).listAll()
        .collectList().block();
    assertNotNull(result);
    assertEquals(list, result);
    assertThat(server, is(MockServer.requestsMatch()));
  }

  @Test
  public void getOne() {
    Dummy item = new Dummy("a");
    MockServer server = MockServer.start(objectMapper);
    String rootUrl = server.rootUrl() + "";
    server.get("/dummies/{id}", 42L)
        .expect()
        .status(HttpStatus.OK)
        .contentType(MediaType.APPLICATION_JSON)
        .body(item)
        .enqueue();

    Dummy result = createRestClient(rootUrl).getOne(42L)
        .block();
    assertNotNull(result);
    assertEquals(item, result);
    assertThat(server, is(MockServer.requestsMatch()));
  }

  @Test
  public void delete() {
    Dummy item = new Dummy("a");
    MockServer server = MockServer.start(objectMapper);
    String rootUrl = server.rootUrl() + "";
    server.delete("/dummies/{id}", 42L)
        .expect()
        .status(HttpStatus.OK)
        .contentType(MediaType.APPLICATION_JSON)
        .body(item)
        .enqueue();

    Dummy result = createRestClient(rootUrl).delete(42L)
        .block();
    assertNotNull(result);
    assertEquals(item, result);
    assertThat(server, is(MockServer.requestsMatch()));
  }

  @Test
  public void create() {
    Dummy item = new Dummy("a");
    MockServer server = MockServer.start(objectMapper);
    String rootUrl = server.rootUrl() + "";
    server.post("/dummies")
        .body(item)
        .expect()
        .status(HttpStatus.CREATED)
        .contentType(MediaType.APPLICATION_JSON)
        .body(item)
        .enqueue();

    Dummy result = createRestClient(rootUrl).create(item)
        .block();
    assertNotNull(result);
    assertEquals(item, result);
    assertThat(server, is(MockServer.requestsMatch()));
  }

  @Test
  public void update() {
    Dummy item = new Dummy("a");
    MockServer server = MockServer.start(objectMapper);
    String rootUrl = server.rootUrl() + "";
    server.put("/dummies/{id}", 42L)
        .body(item)
        .expect()
        .status(HttpStatus.OK)
        .contentType(MediaType.APPLICATION_JSON)
        .body(item)
        .enqueue();

    Dummy result = createRestClient(rootUrl).update(42L, item)
        .block();
    assertNotNull(result);
    assertEquals(item, result);
    assertThat(server, is(MockServer.requestsMatch()));
  }

  static class Dummy {

    private String name;

    public Dummy() {
    }

    public Dummy(String name) {
      this.name = name;
    }

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      Dummy dummy = (Dummy) o;
      return Objects.equals(name, dummy.name);
    }

    @Override
    public int hashCode() {
      return Objects.hash(name);
    }
  }

}