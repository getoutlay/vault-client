package io.gitlab.getoutlay.vault.client;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import okhttp3.HttpUrl;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.util.DefaultUriBuilderFactory;

public class MockServer {

  private final ObjectMapper objectMapper;
  private final MockWebServer server;
  private final HttpUrl rootUrl;
  private final List<MockRequest> requests;
  private HttpStatus httpStatus;
  private MediaType contentType;
  private String body;

  public MockServer(ObjectMapper objectMapper) {
    this.objectMapper = objectMapper;
    this.server = new MockWebServer();
    this.rootUrl = server.url("");
    this.requests = new ArrayList<>();
  }

  public static Matcher<MockServer> requestsMatch() {
    return new MockServerMatcher();
  }

  public static MockServer start(ObjectMapper objectMapper) {
    return new MockServer(objectMapper);
  }

  private static <B> String serialize(ObjectMapper objectMapper, B body) {
    try {
      return objectMapper.writeValueAsString(body);
    } catch (JsonProcessingException e) {
      e.printStackTrace();
      return null;
    }
  }

  public HttpUrl rootUrl() {
    return rootUrl;
  }


  private void addRequest(MockRequest request) {
    requests.add(request);
  }

  private int getRequestCount() {
    return server.getRequestCount();
  }

  private RecordedRequest takeRequest() throws InterruptedException {
    return server.takeRequest();
  }

  private MockResponse enqueue() {
    MockResponse response = new MockResponse();
    if (httpStatus != null) {
      response.setResponseCode(httpStatus.value());
    }
    if (contentType != null) {
      response.setHeader("Content-Type", contentType.toString());
    }
    if (body != null) {
      response.setBody(body);
    }
    server.enqueue(response);
    return response;
  }

  public MockRequestBuilder get(String uri) {
    return request(HttpMethod.GET, uri);
  }

  public MockRequestBuilder get(String uri, Object... uriVariables) {
    return request(HttpMethod.GET, uri, uriVariables);
  }

  public MockRequestBuilder delete(String uri) {
    return request(HttpMethod.DELETE, uri);
  }

  public MockRequestBuilder delete(String uri, Object... uriVariables) {
    return request(HttpMethod.DELETE, uri, uriVariables);
  }

  public MockRequestBuilder post(String uri) {
    return request(HttpMethod.POST, uri);
  }

  public MockRequestBuilder post(String uri, Object... uriVariables) {
    return request(HttpMethod.POST, uri, uriVariables);
  }

  public MockRequestBuilder put(String uri) {
    return request(HttpMethod.PUT, uri);
  }

  public MockRequestBuilder put(String uri, Object... uriVariables) {
    return request(HttpMethod.PUT, uri, uriVariables);
  }

  private MockRequestBuilder request(HttpMethod httpMethod, String uri, Object... uriVariables) {
    if (uriVariables != null && uriVariables.length > 0) {
      uri = new DefaultUriBuilderFactory().expand(uri, uriVariables).toString();
    }
    if (rootUrl.toString().endsWith("/") && (uri != null && uri.startsWith("/"))) {
      uri = uri.substring(1);
    }
    return new MockRequestBuilder(this)
        .method(httpMethod.name())
        .url(uri);
  }

  static class MockRequestBuilder {

    private final MockServer mockServer;
    private String url;
    private String method;
    private String body;

    public MockRequestBuilder(MockServer mockServer) {
      this.mockServer = mockServer;
    }

    public MockRequestBuilder url(String url) {
      this.url = url;
      return this;
    }

    public MockRequestBuilder method(String method) {
      this.method = method;
      return this;
    }

    public <B> MockRequestBuilder body(B body) {
      this.body = serialize(mockServer.objectMapper, body);
      return this;
    }

    public MockResponseBuilder expect() {
      MockRequest request = new MockRequest();
      request.expectedMethod = method;
      request.expectedUrl = url;
      request.expectedBody = body;
      return new MockResponseBuilder(mockServer, request);
    }

  }

  private static class MockRequest {

    String expectedUrl;
    String expectedMethod;
    String expectedBody;
  }

  static class MockResponseBuilder {

    private final MockServer mockServer;
    private HttpStatus status;
    private MediaType contentType;
    private String body;

    public MockResponseBuilder(MockServer mockServer, MockRequest request) {
      this.mockServer = mockServer;
      this.mockServer.addRequest(request);
    }

    public MockResponseBuilder status(HttpStatus status) {
      this.status = status;
      return this;
    }

    public MockResponseBuilder contentType(MediaType contentType) {
      this.contentType = contentType;
      return this;
    }

    public <B> MockResponseBuilder body(B body) {
      this.body = serialize(mockServer.objectMapper, body);
      return this;
    }

    public MockResponse enqueue() {
      mockServer.httpStatus = status;
      mockServer.contentType = contentType;
      mockServer.body = body;
      return mockServer.enqueue();
    }
  }

  static class MockServerMatcher extends TypeSafeMatcher<MockServer> {

    @Override
    protected boolean matchesSafely(MockServer server) {
      List<MockRequest> requests = server.requests;
      if (server.getRequestCount() == requests.size()) {
        for (MockRequest request : requests) {
          try {
            RecordedRequest recorded = server.takeRequest();
            HttpUrl requestUrl = recorded.getRequestUrl();
            String expectedUrl = server.rootUrl + request.expectedUrl;
            String expectedBody = request.expectedBody == null ? "" : request.expectedBody;
            assertNotNull(requestUrl);
            assertEquals(expectedUrl, requestUrl.toString());
            assertEquals(request.expectedMethod, recorded.getMethod());
            assertEquals(expectedBody, recorded.getBody().readUtf8());
          } catch (InterruptedException e) {
            return false;
          }
        }
        return true;
      }
      return false;
    }

    @Override
    public void describeTo(Description description) {
      description.appendText("Server requests match");
    }
  }

}
