package io.gitlab.getoutlay.vault.client;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@Configuration
@EnableConfigurationProperties(VaultClientConfigurationProperties.class)
public class TestApplication {

}
