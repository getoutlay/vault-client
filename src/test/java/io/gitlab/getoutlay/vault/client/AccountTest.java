package io.gitlab.getoutlay.vault.client;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class AccountTest {

  @Test
  void getIdSetId() {
    Account account = new Account();
    assertNull(account.getId());

    account.setId(42L);
    assertEquals(42L, account.getId());
  }

}