package io.gitlab.getoutlay.vault.client;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ActiveProfiles;
import reactor.core.publisher.Mono;

@SpringBootTest(webEnvironment = WebEnvironment.NONE)
@ActiveProfiles("test")
class VaultClientTest {

  @Autowired
  VaultClientConfiguration configuration;

  VaultClient vaultClient;

  @BeforeEach
  public void setup() {
    vaultClient = new VaultClient(configuration);
  }

  @Test
  public void vaultClientCanBeCreated() {
    assertNotNull(vaultClient);
  }

  @Test
  public void account() {
    RestClient<Account> accountClient = vaultClient.account();
    assertNotNull(accountClient);
  }

  @Test
  public void accountTransactions() {
    RestClient<Transaction> transactions = vaultClient.account(42L).transactions();
    assertNotNull(transactions);
  }

  @Test
  public void transaction() {
    RestClient<Transaction> transactionClient = vaultClient.transaction();
    assertNotNull(transactionClient);
  }

  @Test
  public void transactionAccount() {
    Mono<Account> account = vaultClient.transaction(42L).account();
    assertNotNull(account);
  }

  @Test
  public void category() {
    RestClient<Category> categoryClient = vaultClient.category();
    assertNotNull(categoryClient);
  }
}