package io.gitlab.getoutlay.vault.client;

import static io.gitlab.getoutlay.vault.client.StringUtils.toLowerHyphen;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class StringUtilsTest {

  @Test
  public void toLowerHyphenWithNull() {
    assertEquals("", toLowerHyphen(null));
  }
  @Test
  public void toLowerHyphenWithEmptyString() {
    assertEquals("", toLowerHyphen(""));
  }

  @Test
  public void toLowerHyphenWithNumbersCombined() {
    assertEquals("asd123", toLowerHyphen("asd123"));
    assertEquals("123asd123", toLowerHyphen("123asd123"));
  }

  @Test
  public void toLowerHyphenWithStringStartingWithUppercase() {
    assertEquals("some-string", toLowerHyphen("SomeString"));
  }

  @Test
  public void toLowerHyphenWithComplexString() {
    assertEquals("some-complex-string-123one-two-three", toLowerHyphen("someComplexString-123oneTwoThree"));
  }
}