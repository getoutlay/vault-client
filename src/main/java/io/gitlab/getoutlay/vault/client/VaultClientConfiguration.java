package io.gitlab.getoutlay.vault.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

@Configuration
public class VaultClientConfiguration {

  private final Logger logger = LoggerFactory.getLogger(getClass());
  private final VaultClientConfigurationProperties properties;
  private final ObjectMapper objectMapper;

  @Autowired
  public VaultClientConfiguration(VaultClientConfigurationProperties properties, ObjectMapper objectMapper) {
    this.properties = properties;
    this.objectMapper = objectMapper;
  }

  public VaultClientConfigurationProperties getProperties() {
    return properties;
  }

  public ObjectMapper getObjectMapper() {
    return objectMapper;
  }

  public String getVaultServiceUrl() {
    return properties.getUrl();
  }
}
