package io.gitlab.getoutlay.vault.client;

import static io.gitlab.getoutlay.vault.client.StringUtils.toLowerHyphen;

import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

public class EntityReference {

  protected final Object id;

  public <ID> EntityReference(ID id) {
    this.id = id;
  }

  protected <T> Mono<T> getOne(WebClient client, Class<T> targetType, String fromNamespace, String targetNamespace) {
    return client.get()
        .uri("/{from}/{id}/${target}", toLowerHyphen(fromNamespace), id, toLowerHyphen(targetNamespace))
        .accept(MediaType.APPLICATION_JSON)
        .retrieve()
        .bodyToMono(targetType);
  }

}
