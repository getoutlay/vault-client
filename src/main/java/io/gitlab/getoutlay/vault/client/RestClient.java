package io.gitlab.getoutlay.vault.client;

import static org.springframework.web.reactive.function.BodyInserters.fromValue;

import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class RestClient<T> {

  private final WebClient client;
  private final String nameSpace;
  private final Class<T> type;

  public RestClient(WebClient client, Class<T> type, String nameSpace) {
    this.client = client;
    this.nameSpace = nameSpace;
    this.type = type;
  }

  public Flux<T> listAll() {
    return client.get()
        .uri(nameSpace)
        .accept(MediaType.APPLICATION_JSON)
        .retrieve()
        .bodyToFlux(type);
  }

  public Mono<T> getOne(Long id) {
    return client.get()
        .uri(nameSpace + "/{id}", id)
        .accept(MediaType.APPLICATION_JSON)
        .retrieve()
        .bodyToMono(type);
  }

  public Mono<T> delete(Long id) {
    return client.delete()
        .uri(nameSpace + "/{id}", id)
        .accept(MediaType.APPLICATION_JSON)
        .retrieve()
        .bodyToMono(type);
  }

  public Mono<T> create(T entity) {
    return client.post()
        .uri(nameSpace)
        .accept(MediaType.APPLICATION_JSON)
        .body(fromValue(entity))
        .retrieve()
        .bodyToMono(type);
  }

  public Mono<T> update(Long id, T entity) {
    return client.put()
        .uri(nameSpace + "/{id}", id)
        .accept(MediaType.APPLICATION_JSON)
        .body(fromValue(entity))
        .retrieve()
        .bodyToMono(type);
  }
}
