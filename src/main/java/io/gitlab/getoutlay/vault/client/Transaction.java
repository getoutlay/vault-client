package io.gitlab.getoutlay.vault.client;

import java.time.Instant;

public class Transaction {

  private Long id;
  private Long accountId;
  private String desription;
  private Double amount;
  private Currency currency;
  private Instant bookedOn;
  private Instant executedOn;
  private String explanation;
  private String reference;
  private String corresponderName;
  private String corresponderBank;
  private String corresponderAccount;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getAccountId() {
    return accountId;
  }

  public void setAccountId(Long accountId) {
    this.accountId = accountId;
  }

  public String getDesription() {
    return desription;
  }

  public void setDesription(String desription) {
    this.desription = desription;
  }

  public Double getAmount() {
    return amount;
  }

  public void setAmount(Double amount) {
    this.amount = amount;
  }

  public Currency getCurrency() {
    return currency;
  }

  public void setCurrency(Currency currency) {
    this.currency = currency;
  }

  public Instant getBookedOn() {
    return bookedOn;
  }

  public void setBookedOn(Instant bookedOn) {
    this.bookedOn = bookedOn;
  }

  public Instant getExecutedOn() {
    return executedOn;
  }

  public void setExecutedOn(Instant executedOn) {
    this.executedOn = executedOn;
  }

  public String getExplanation() {
    return explanation;
  }

  public void setExplanation(String explanation) {
    this.explanation = explanation;
  }

  public String getReference() {
    return reference;
  }

  public void setReference(String reference) {
    this.reference = reference;
  }

  public String getCorresponderName() {
    return corresponderName;
  }

  public void setCorresponderName(String corresponderName) {
    this.corresponderName = corresponderName;
  }

  public String getCorresponderBank() {
    return corresponderBank;
  }

  public void setCorresponderBank(String corresponderBank) {
    this.corresponderBank = corresponderBank;
  }

  public String getCorresponderAccount() {
    return corresponderAccount;
  }

  public void setCorresponderAccount(String corresponderAccount) {
    this.corresponderAccount = corresponderAccount;
  }

}
