package io.gitlab.getoutlay.vault.client;

import static org.springframework.http.MediaType.APPLICATION_JSON;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.codec.ClientCodecConfigurer.ClientDefaultCodecs;
import org.springframework.http.codec.json.Jackson2JsonDecoder;
import org.springframework.http.codec.json.Jackson2JsonEncoder;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

public class VaultClient {

  private final WebClient client;

  public VaultClient(VaultClientConfiguration config) {
    ExchangeStrategies strategies = ExchangeStrategies.builder()
        .codecs(clientCodecs -> {
          ClientDefaultCodecs defaultCodecs = clientCodecs.defaultCodecs();
          ObjectMapper objectMapper = config.getObjectMapper();
          defaultCodecs.jackson2JsonEncoder(new Jackson2JsonEncoder(objectMapper, APPLICATION_JSON));
          defaultCodecs.jackson2JsonDecoder(new Jackson2JsonDecoder(objectMapper, APPLICATION_JSON));
        })
        .build();
    client = WebClient.builder()
        .exchangeStrategies(strategies)
        .baseUrl(config.getVaultServiceUrl())
        .build();
  }

  public RestClient<Account> account() {
    return new RestClient<>(client, Account.class, "accounts");
  }

  public RestClient<Transaction> transaction() {
    return new RestClient<>(client, Transaction.class, "transactions");
  }

  public RestClient<Category> category() {
    return new RestClient<>(client, Category.class, "categories");
  }

  public <ID> AccountReference account(ID id) {
    return new AccountReference(id);
  }

  public <ID> TransactionReference transaction(ID id) {
    return new TransactionReference(id);
  }

  class AccountReference extends EntityReference {

    public <ID> AccountReference(ID id) {
      super(id);
    }

    public RestClient<Transaction> transactions() {
      return new RestClient<>(client, Transaction.class, "accounts");
    }
  }

  class TransactionReference extends EntityReference {

    public <ID> TransactionReference(ID id) {
      super(id);
    }

    public Mono<Account> account() {
      return getOne(client, Account.class, "transactions", "accounts");
    }
  }
}
