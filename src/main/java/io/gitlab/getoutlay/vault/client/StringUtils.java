package io.gitlab.getoutlay.vault.client;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {

  public static String toLowerHyphen(String name) {
    if (name == null || "".equals(name)) {
      return "";
    }
    final Pattern pattern = Pattern.compile("(?=[A-Z][a-z])");
    final Matcher matcher = pattern.matcher(name);
    String replaced = matcher.replaceAll(match -> {
      if (match.start() > 0) {
        return "-";
      }
      return "";
    });
    return replaced.toLowerCase(Locale.ENGLISH);
  }


}
