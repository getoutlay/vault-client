package io.gitlab.getoutlay.vault.client;

public class Account {

  String name;
  String institution;
  String accountNumber;
  boolean expenseAccount;
  Currency currency;
  private Long id;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getInstitution() {
    return institution;
  }

  public void setInstitution(String institution) {
    this.institution = institution;
  }

  public String getAccountNumber() {
    return accountNumber;
  }

  public void setAccountNumber(String accountNumber) {
    this.accountNumber = accountNumber;
  }

  public Boolean getExpenseAccount() {
    return expenseAccount;
  }

  public void setExpenseAccount(Boolean expenseAccount) {
    this.expenseAccount = expenseAccount;
  }

  public Currency getCurrency() {
    return currency;
  }

  public void setCurrency(Currency currency) {
    this.currency = currency;
  }


}
